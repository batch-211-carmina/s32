let http = require("http");

let port = 4000;

let server = http.createServer(function (request, response) {
	
	//GET homepage route
	if (request.url == '/' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}
	
	//GET Profile route
	if (request.url == '/profile' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile');
	}

	//GET Course route
	if (request.url == '/courses' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's our courses available.");
	}

	//POST Course route
	if (request.url == '/addCourse' && request.method == 'POST') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add course to our resources.');
	}

	//PUT Course route
	if (request.url == '/updateCourse' && request.method == 'PUT') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Update a course to our resources.');
	}

	//DELETE Course route
	if (request.url == '/archiveCourse' && request.method == 'DELETE') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Archive course to our resources.');
	}
});

server.listen(4000);

console.log(`Server running at localhost: ${port}`);